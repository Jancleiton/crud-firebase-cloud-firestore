import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { PessoaService } from 'src/app/services/pessoa.service';
import { Pessoa } from 'src/app/models/pessoa.model';

@Component({
  selector: 'app-grafico',
  templateUrl: './grafico.component.html',
  styleUrls: ['./grafico.component.css']
})
export class GraficoComponent implements OnInit, OnChanges {

  @Input() atualizar
  carregando: boolean = true
  totalHomens: number = 0
  totalMulheres: number = 0
  totalNaoDefinido: number = 0
  labels: string[] = ['Homens', 'Mulheres', 'Não informado']
  dados: number[]
  tipo: string = 'doughnut'
  opcoes = {legend: { position: 'right' }}
  cores = [
    {
      backgroundColor: ['#004159', '#0052A5', '#00C590'],       
    }
  ]

  constructor(private pessoaService: PessoaService) { }

  ngOnInit(): void {
    this.listarPessoas()
  }

  ngOnChanges() {
    this.listarPessoas()
  }

  listarPessoas() {
    this.carregando = true
    this.pessoaService.listar().then((pessoas: Pessoa[]) => {
      let filtroHomens = pessoa => pessoa.sexo === 'm'
      let filtroMulheres = pessoa => pessoa.sexo === 'f'
      let filtroNaoInformado = pessoa => pessoa.sexo === '-'
      this.totalHomens = pessoas.filter(filtroHomens).length
      this.totalMulheres = pessoas.filter(filtroMulheres).length
      this.totalNaoDefinido = pessoas.filter(filtroNaoInformado).length
      this.dados = [this.totalHomens, this.totalMulheres, this.totalNaoDefinido]
      this.carregando = false
    })
  }

}
