import { Component, OnInit, Input } from '@angular/core';
import { Pessoa } from 'src/app/models/pessoa.model';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PessoaService } from 'src/app/services/pessoa.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-modal-confirmacao',
  templateUrl: './modal-confirmacao.component.html',
  styleUrls: ['./modal-confirmacao.component.css']
})
export class ModalConfirmacaoComponent implements OnInit {

  @Input() mensagem = ''

  modalResultado: Subject<boolean>

  constructor(private modalRef: BsModalRef) { }

  ngOnInit(): void {
    this.modalResultado = new Subject()
  }

  excluir(){
    this.excluirOuFechar(true)
  }

  fechar(){
    this.excluirOuFechar(false)
  }

  excluirOuFechar(resultado: boolean){
    this.modalResultado.next(resultado)
    this.modalRef.hide()
  }

}
