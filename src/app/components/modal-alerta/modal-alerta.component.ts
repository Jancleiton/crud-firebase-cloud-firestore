import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-alerta',
  templateUrl: './modal-alerta.component.html',
  styleUrls: ['./modal-alerta.component.css']
})
export class ModalAlertaComponent implements OnInit {

  @Input() mensagem = ''
  @Input() alertClass = ''  

  constructor(private modalRef: BsModalRef) { }

  ngOnInit(){
    setTimeout(() => this.modalRef.hide(), 3500 )
  }  

}
