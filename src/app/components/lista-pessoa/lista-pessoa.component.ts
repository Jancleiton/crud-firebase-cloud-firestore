import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Pessoa } from 'src/app/models/pessoa.model';
import { PessoaService } from 'src/app/services/pessoa.service';
import { ModalService } from 'src/app/services/modal.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-lista-pessoa',
  templateUrl: './lista-pessoa.component.html',
  styleUrls: ['./lista-pessoa.component.css']
})
export class ListaPessoaComponent implements OnInit, OnChanges {

  @Input() atualizar
  @Output() excluiu: EventEmitter<number> = new EventEmitter()
  @Output() atualizarPessoa: EventEmitter<number> = new EventEmitter()
  @Output() quantidadeCadastros: EventEmitter<number> = new EventEmitter()
  pessoas: Pessoa[]
  carregando: boolean = true  

  constructor(
    private pessoaService: PessoaService,
    private modalService: ModalService) { }

  ngOnInit(): void {
    this.listarPessoas()
  }

  ngOnChanges() {
    this.listarPessoas()
  }  

  listarPessoas() {
    this.carregando = true
    this.pessoaService.listar()
      .then(resp => {
        this.pessoas = resp
        this.quantidadeCadastros.emit(this.pessoas.length)
        this.carregando = false
      })
      .catch((erro) => this.modalService.alertaErro(`Erro: ${erro}`))
  }

  excluir(pessoa: Pessoa) {
    const mensagem = `Deseja realmente excluir ${pessoa.nome.toUpperCase()}?`
    const modalConfirmacaoSubject: Subject<boolean> = this.modalService.confirmacao(mensagem)

    modalConfirmacaoSubject.subscribe(result => {
      if(result){
        this.pessoaService.excluir(pessoa.id)
        .then(() => this.exclusaoFinalizada(pessoa))
        .catch((erro) => this.modalService.alertaErro(`Erro: ${erro}`))
      }
    })    
  }

  exclusaoFinalizada(pessoa: Pessoa) {
    this.modalService.alertaSucesso(`${pessoa.nome.toUpperCase()} foi removido(a).`)    
    this.excluiu.emit(Date.now())
  }

  enviarPessoaParaFormulario(pessoa){
    this.atualizarPessoa.emit(pessoa)
  }
  
}
