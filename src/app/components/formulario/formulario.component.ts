import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PessoaService } from '../../services/pessoa.service';
import { Pessoa } from 'src/app/models/pessoa.model';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit, OnChanges {

  @Output() cadastrou: EventEmitter<number> = new  EventEmitter()
  @Input() pessoaParaAtualizar: Pessoa = null
  @Input() quantidadeCadastros = 0

  dataMax: Date 

  formulario = new FormGroup({
    nome: new FormControl('', Validators.required),
    dataNascimento: new FormControl(this.dataMax, Validators.required),
    sexo: new FormControl('', Validators.required)
  })

  constructor(
    private pessoaService: PessoaService,
    private modalService: ModalService,
    private localeService: BsLocaleService
    ) {  }

  ngOnInit(): void {        
    this.formatarDatePicker()

  }

  ngOnChanges(){
    this.atualizarFormulario()
  }  

  formatarDatePicker(){
    this.localeService.use('pt-br')
    this.dataMax = new Date()
    this.dataMax.setFullYear(this.dataMax.getFullYear()-18)        
  }

  atualizarFormulario(){
    if(this.pessoaParaAtualizar){
      this.formulario.get('nome').setValue(this.pessoaParaAtualizar.nome)
      this.formulario.get('dataNascimento').setValue(new Date(this.pessoaParaAtualizar.dataNascimento))
      this.formulario.get('sexo').setValue(this.pessoaParaAtualizar.sexo)      
    }
  }

  cadastrarOuAtualizar(){
    this.pessoaParaAtualizar? this.atualizar() : this.cadastrar()    
  }

  atualizar(){
    this.pessoaParaAtualizar.nome = this.formulario.get('nome').value
    this.pessoaParaAtualizar.dataNascimento = new Date(this.formulario.get('dataNascimento').value).getTime()
    this.pessoaParaAtualizar.sexo = this.formulario.get('sexo').value
    this.pessoaService.atualizar(this.pessoaParaAtualizar)
      .then(() => this.cadastroAtualizacaoFinalizado())
      .catch((erro) => this.modalService.alertaErro(`Erro: ${erro}`))
  }

  cadastrar(){    
    if(this.podeCadastrar()){
      let dataNaoFormatada = this.formulario.get('dataNascimento').value
      this.formulario.get('dataNascimento').setValue(new Date(dataNaoFormatada).getTime())    
      this.pessoaService.cadastrar(this.formulario.value)
        .then(() => this.cadastroAtualizacaoFinalizado())
        .catch((erro) => this.modalService.alertaErro(`Erro: ${erro}`))  
    } else {
      this.modalService.alertaErro('Exclua algum registo para adicionar um novo.')      
    }             
    
  }

  podeCadastrar(): boolean{
    return this.quantidadeCadastros < 5 
  }

  cadastroAtualizacaoFinalizado(){    
    this.cadastrou.emit(Date.now())
    this.formulario.reset()   
    this.pessoaParaAtualizar = null 
    this.modalService.alertaSucesso('Operação realizada com sucesso.')    
  }  

  cancelar(){
    this.pessoaParaAtualizar = null
  }
}
