import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreCollection } from '@angular/fire/firestore/public_api';
import { plainToClass, classToPlain } from "class-transformer";
import { Pessoa } from '../models/pessoa.model';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  colecao: AngularFirestoreCollection

  constructor(
    private firestore: AngularFirestore
  ) {
    this.colecao = this.firestore.collection<Pessoa>("pessoas")
  }  

  cadastrar(pessoa: Pessoa): Promise<any> {  
    pessoa.nome = pessoa.nome.toLowerCase()
    pessoa.dataNascimento = parseInt(pessoa.dataNascimento.toString())
    return new Promise((resolve, reject) => {
      this.colecao.add(classToPlain(pessoa))
        .then(() => { resolve() })
        .catch((erro) => reject(erro))
    })
  }

  atualizar(pessoa: Pessoa): Promise<any>{  
    pessoa.nome = pessoa.nome.toLowerCase()
    pessoa.dataNascimento = parseInt(pessoa.dataNascimento.toString())    
    return new Promise((resolve, reject) => {      
      this.colecao.doc(pessoa.id).set(classToPlain(pessoa))
        .then(() => resolve())
        .catch((erro) => reject(erro))
    })
  }

  listar(): Promise<any> {
    let pessoas = new Array<Pessoa>()
    return new Promise((resolve, reject) => {
      this.colecao.ref.orderBy('nome').get()
      .then(colecao => {        
        colecao.forEach(doc => {
          let pessoa = plainToClass(Pessoa, doc.data())
          pessoa.id = doc.id
          pessoas.push(pessoa)
        })
        resolve(pessoas)
      })
      .catch((erro) => reject(erro))      
    })
  }

  excluir(id: string): Promise<any> {        
    return new Promise((resolve, reject) => {
      this.colecao.doc(id).delete()
        .then(() => resolve())
        .catch(erro => reject(erro)) 
    })
  }
}
