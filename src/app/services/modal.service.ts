import { Injectable } from '@angular/core';
import { Pessoa } from '../models/pessoa.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalConfirmacaoComponent } from '../components/modal-confirmacao/modal-confirmacao.component';
import { Subject } from 'rxjs';
import { ModalAlertaComponent } from '../components/modal-alerta/modal-alerta.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modalRef: BsModalRef

  constructor(
    private modalService: BsModalService
  ) { }

  confirmacao(mensagem: string): Subject<boolean> {
    this.modalRef = this.modalService.show(ModalConfirmacaoComponent, {class: 'modal-sm'})
    this.modalRef.content.mensagem = mensagem
    return this.modalRef.content.modalResultado
  }

  private alerta(mensagem: string, alertClass: string){
    this.modalRef = this.modalService.show(ModalAlertaComponent)
    this.modalRef.content.mensagem = mensagem
    this.modalRef.content.alertClass = alertClass    
  }

  alertaSucesso(mensagem: string){
    this.alerta(mensagem, 'success')
  }

  alertaErro(mensagem: string){
    this.alerta(mensagem, 'danger')
  }

}
