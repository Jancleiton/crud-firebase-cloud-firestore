import { Component } from '@angular/core';
import { Pessoa } from './models/pessoa.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  atualizarLista = 0
  pessoaParaAtualizar: Pessoa = null
  quantidadeCadastros = 0

  atualizarListaGatilho(timeStampCadastro: number){
    this.atualizarLista = timeStampCadastro
  }

  atualizaQuantidadeCadastros(quantidade){
    this.quantidadeCadastros = quantidade
  }

  enviarParaFormulario(pessoa: Pessoa){
    this.pessoaParaAtualizar = pessoa
  }
}
