import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment} from '../environments/environment'
import { AppComponent } from './app.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { SobreComponent } from './components/sobre/sobre.component';
import { ModalConfirmacaoComponent } from './components/modal-confirmacao/modal-confirmacao.component';
import { GraficoComponent } from './components/grafico/grafico.component';
import { PessoaService } from './services/pessoa.service';
import { ListaPessoaComponent } from './components/lista-pessoa/lista-pessoa.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
//ngx-bootstrap
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalService } from './services/modal.service';
import { ModalAlertaComponent } from './components/modal-alerta/modal-alerta.component';

@NgModule({
  declarations: [
    AppComponent,
    FormularioComponent,
    ListaPessoaComponent,
    GraficoComponent,
    SobreComponent,
    ModalConfirmacaoComponent,
    ModalAlertaComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    ChartsModule,
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [
    PessoaService,
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary){
    library.addIconPacks(fas);
    defineLocale('pt-br', ptBrLocale);
  }
 }
