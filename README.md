# CrudFirebaseCloudFirestore

>Projeto desenvolvido para estudar o Firestore com funções básicas para cadastrar, excluir, listar e atualizar dados.

# Foi usado nesse projeto

[Angular](https://angular.io/)
[Bootstrap 4](https://getbootstrap.com/)
[Ngx-bootstrap](https://valor-software.com/ngx-bootstrap/#/)
[Firebase/Firestore](https://firebase.google.com/)
[Ng2-charts](https://valor-software.com/ng2-charts/)


